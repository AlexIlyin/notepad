package com.alexilyin.android.demo.notepad.data.file;

import android.annotation.TargetApi;
import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.util.Log;

import com.alexilyin.android.demo.notepad.common.MockApp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by alexilyin on 16.10.16.
 */
public class StorageHelperTest {

    private static final String TAG = StorageHelperTest.class.getSimpleName();

    private static final String TEST_DATA = "12345667890a";
    private static final String OVERRIDE_TEST_DATA = "abcdef0987654321";
    private Context context;

    private Random random = new Random();
    //    private String testFilename = "_com.alexilyin.android_StorageHelper_testfile_" + random.nextInt() + ".tmp";
//    private String testSubfolder = "_com.alexilyin.android_StorageHelper_testfolder_" + random.nextInt();
    private String testFilename = "testfile_" + Math.abs(random.nextInt()) + ".tmp";
    private String testSubfolder = "testfolder_" + Math.abs(random.nextInt());
    private String testLocalFilePath = testSubfolder + "/" + testFilename;
    File testFolder;

    @Before
    public void setUp() throws Exception {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        MockApp app = (MockApp) instrumentation.getTargetContext().getApplicationContext();
        context = app.component().context();
    }

    @After
    public void tearDown() throws Exception {

//        // cleanup the mess
//        for (File f : testFolder.getParentFile().listFiles((dir, name) -> name.startsWith("testfolder_"))) {
//            for (File sf : f.listFiles()) sf.delete();
//            f.delete();
//        }

        if (testFolder != null && testFolder.exists()) {
            for (File f : testFolder.listFiles()) {
                f.delete();
            }
            testFolder.delete();
        }
    }


    
    // =======================================================================================
    // Specific tests
    // =======================================================================================

    // Context.getFilesDir()
    @Test
    public void testNormalObjectWriteAndRead_FilesDir() throws Exception {

        testFolder = makeTestfolderFile(context.getFilesDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getFilesDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteAndRead_FilesDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToFilesDir(context, testLocalFilePath, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromFilesDir(context, testLocalFilePath);
        checkReadResult(TEST_DATA, readObject);
    }

    @Test
    public void testNormalObjectWriteOverrideAndRead_FilesDir() throws Exception {

        testFolder = makeTestfolderFile(context.getFilesDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getFilesDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteOverrideAndRead_FilesDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToFilesDir(context, testLocalFilePath, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromFilesDir(context, testLocalFilePath);
        checkReadResult(TEST_DATA, readObject);

        // override action
        StorageHelper.writeObjectToFilesDir(context, testLocalFilePath, OVERRIDE_TEST_DATA);
        checkWriteResult(OVERRIDE_TEST_DATA, testFile);

        // read action
        readObject = StorageHelper.readObjectFromFilesDir(context, testLocalFilePath);
        checkReadResult(OVERRIDE_TEST_DATA, readObject);
    }

    @Test
    public void testNullObjectWriteAndRead_FilesDir() throws Exception {

        testFolder = makeTestfolderFile(context.getFilesDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getFilesDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNullObjectWriteAndRead_FilesDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToFilesDir(context, testLocalFilePath, null);
        checkWriteResult(null, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromFilesDir(context, testLocalFilePath);
        checkReadResult(null, readObject);
    }


    // Context.getCacheDir()
    @Test
    public void testNormalObjectWriteAndRead_CacheDir() throws Exception {

        testFolder = makeTestfolderFile(context.getCacheDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getCacheDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteAndRead_CacheDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToCacheDir(context, testLocalFilePath, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromCacheDir(context, testLocalFilePath);
        checkReadResult(TEST_DATA, readObject);
    }

    @Test
    public void testNormalObjectWriteOverrideAndRead_CacheDir() throws Exception {

        testFolder = makeTestfolderFile(context.getCacheDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getCacheDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteOverrideAndRead_CacheDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToCacheDir(context, testLocalFilePath, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromCacheDir(context, testLocalFilePath);
        checkReadResult(TEST_DATA, readObject);

        // override action
        StorageHelper.writeObjectToCacheDir(context, testLocalFilePath, OVERRIDE_TEST_DATA);
        checkWriteResult(OVERRIDE_TEST_DATA, testFile);

        // read action
        readObject = StorageHelper.readObjectFromCacheDir(context, testLocalFilePath);
        checkReadResult(OVERRIDE_TEST_DATA, readObject);
    }

    @Test
    public void testNullObjectWriteAndRead_CacheDir() throws Exception {

        testFolder = makeTestfolderFile(context.getCacheDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getCacheDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNullObjectWriteAndRead_CacheDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToCacheDir(context, testLocalFilePath, null);
        checkWriteResult(null, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromCacheDir(context, testLocalFilePath);
        checkReadResult(null, readObject);
    }

    // Context.getCodeCacheDir()
    @Test
    @TargetApi(21)
    @SdkSuppress(minSdkVersion=21)
    public void testNormalObjectWriteAndRead_CodeCacheDir() throws Exception {

        testFolder = makeTestfolderFile(context.getCodeCacheDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getCodeCacheDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteAndRead_CodeCacheDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToCodeCacheDir(context, testLocalFilePath, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromCodeCacheDir(context, testLocalFilePath);
        checkReadResult(TEST_DATA, readObject);
    }

    @Test
    @TargetApi(21)
    @SdkSuppress(minSdkVersion=21)
    public void testNormalObjectWriteOverrideAndRead_CodeCacheDir() throws Exception {

        testFolder = makeTestfolderFile(context.getCodeCacheDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getCodeCacheDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteOverrideAndRead_CodeCacheDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToCodeCacheDir(context, testLocalFilePath, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromCodeCacheDir(context, testLocalFilePath);
        checkReadResult(TEST_DATA, readObject);

        // override action
        StorageHelper.writeObjectToCodeCacheDir(context, testLocalFilePath, OVERRIDE_TEST_DATA);
        checkWriteResult(OVERRIDE_TEST_DATA, testFile);

        // read action
        readObject = StorageHelper.readObjectFromCodeCacheDir(context, testLocalFilePath);
        checkReadResult(OVERRIDE_TEST_DATA, readObject);
    }

    @Test
    @TargetApi(21)
    @SdkSuppress(minSdkVersion=21)
    public void testNullObjectWriteAndRead_CodeCacheDir() throws Exception {

        testFolder = makeTestfolderFile(context.getCodeCacheDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getCodeCacheDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNullObjectWriteAndRead_CodeCacheDir: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObjectToCodeCacheDir(context, testLocalFilePath, null);
        checkWriteResult(null, testFile);

        // read action
        Object readObject = StorageHelper.readObjectFromCodeCacheDir(context, testLocalFilePath);
        checkReadResult(null, readObject);
    }

    // Context.getDir()
//    @Test
//    public void testNormalObjectWriteAndRead_Dir() throws Exception {
//
//        testFolder = makeTestfolderFile(context.getDir(), testSubfolder);
//        File testFile = makeTestfileFile(context.getDir(), testSubfolder, testFilename);
//
//        Log.d(TAG, "testNormalObjectWriteAndRead_Dir: Test file: " + testFile.getAbsolutePath());
//
//        // prepare
//        checkFileDoesNotExist(testFile);
//
//        // write action
//        StorageHelper.writeObjectToDir(context, testLocalFilePath, TEST_DATA);
//        checkWriteResult(TEST_DATA, testFile);
//
//        // read action
//        Object readObject = StorageHelper.readObjectFromDir(context, testLocalFilePath);
//        checkReadResult(TEST_DATA, readObject);
//    }
//
//    @Test
//    public void testNormalObjectWriteOverrideAndRead_Dir() throws Exception {
//
//        testFolder = makeTestfolderFile(context.getDir(), testSubfolder);
//        File testFile = makeTestfileFile(context.getDir(), testSubfolder, testFilename);
//
//        Log.d(TAG, "testNormalObjectWriteOverrideAndRead_Dir: Test file: " + testFile.getAbsolutePath());
//
//        // prepare
//        checkFileDoesNotExist(testFile);
//
//        // write action
//        StorageHelper.writeObjectToDir(context, testLocalFilePath, TEST_DATA);
//        checkWriteResult(TEST_DATA, testFile);
//
//        // read action
//        Object readObject = StorageHelper.readObjectFromDir(context, testLocalFilePath);
//        checkReadResult(TEST_DATA, readObject);
//
//        // override action
//        StorageHelper.writeObjectToDir(context, testLocalFilePath, OVERRIDE_TEST_DATA);
//        checkWriteResult(OVERRIDE_TEST_DATA, testFile);
//
//        // read action
//        readObject = StorageHelper.readObjectFromDir(context, testLocalFilePath);
//        checkReadResult(OVERRIDE_TEST_DATA, readObject);
//    }
//
//    @Test
//    public void testNullObjectWriteAndRead_Dir() throws Exception {
//
//        testFolder = makeTestfolderFile(context.getDir(), testSubfolder);
//        File testFile = makeTestfileFile(context.getDir(), testSubfolder, testFilename);
//
//        Log.d(TAG, "testNullObjectWriteAndRead_Dir: Test file: " + testFile.getAbsolutePath());
//
//        // prepare
//        checkFileDoesNotExist(testFile);
//
//        // write action
//        StorageHelper.writeObjectToDir(context, testLocalFilePath, null);
//        checkWriteResult(null, testFile);
//
//        // read action
//        Object readObject = StorageHelper.readObjectFromDir(context, testLocalFilePath);
//        checkReadResult(null, readObject);
//    }


    // =======================================================================================
    // Common tests
    // =======================================================================================

    @Test
    public void testNormalObjectWriteAndRead() throws Exception {

        testFolder = makeTestfolderFile(context.getFilesDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getFilesDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteAndRead: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObject(testFile, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObject(testFile);
        checkReadResult(TEST_DATA, readObject);
    }

    @Test
    public void testNormalObjectWriteOverrideAndRead() throws Exception {

        testFolder = makeTestfolderFile(context.getFilesDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getFilesDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNormalObjectWriteOverrideAndRead: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObject(testFile, TEST_DATA);
        checkWriteResult(TEST_DATA, testFile);

        // read action
        Object readObject = StorageHelper.readObject(testFile);
        checkReadResult(TEST_DATA, readObject);

        // override action
        StorageHelper.writeObject(testFile, OVERRIDE_TEST_DATA);
        checkWriteResult(OVERRIDE_TEST_DATA, testFile);

        // read action
        readObject = StorageHelper.readObject(testFile);
        checkReadResult(OVERRIDE_TEST_DATA, readObject);
    }

    @Test
    public void testNullObjectWriteAndRead() throws Exception {

        testFolder = makeTestfolderFile(context.getFilesDir(), testSubfolder);
        File testFile = makeTestfileFile(context.getFilesDir(), testSubfolder, testFilename);

        Log.d(TAG, "testNullObjectWriteAndRead: Test file: " + testFile.getAbsolutePath());

        // prepare
        checkFileDoesNotExist(testFile);

        // write action
        StorageHelper.writeObject(testFile, null);
        checkWriteResult(null, testFile);

        // read action
        Object readObject = StorageHelper.readObject(testFile);
        checkReadResult(null, readObject);
    }



    // =======================================================================================
    // Check service methods
    // =======================================================================================

    private static void checkFileDoesNotExist(File file) {
        if (file.exists()) assertTrue(file.delete());
        assertFalse(file.exists());
    }

    private static void checkWriteResult(String data, File file) {
        if (data != null) {
            assertTrue(file.exists());
            assertTrue(file.length() > 0);
        }
    }

    private static void checkReadResult(String data, Object resultObject) {
        if (data == null) {
            assertNull(resultObject);
        } else {
            assertTrue(resultObject instanceof String);
            String result = (String) resultObject;
            assertTrue(data.equals(result));
        }
    }



    // =======================================================================================
    // Path and File generation service methods
    // =======================================================================================

    private static File makeTestfolderFile(File rootFolder, String testSubfolder) {
        return new File(makeTestfolderPath(rootFolder, testSubfolder));
    }

    private static File makeTestfileFile(File rootFolder, String testSubfolder, String filename) {
        return new File(makeTestfilePath(rootFolder, testSubfolder, filename));
    }

    private static String makeTestfolderPath(File rootFolder, String testSubfolder) {
        return rootFolder.getAbsolutePath() + "/" + testSubfolder;
    }

    private static String makeTestfilePath(File rootFolder, String testSubfolder, String filename) {
        return rootFolder.getAbsolutePath() + "/" + testSubfolder + "/" + filename;
    }

}