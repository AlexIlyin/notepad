package com.alexilyin.android.demo.notepad.common;

import android.util.Log;

/**
 * Created by alexilyin on 13.10.16.
 */

public class MockApp extends App {

    private static final String TAG = App.class.getSimpleName();

    public void setComponent(AppComponent component) {
        this.component = component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }
}
