package com.alexilyin.android.demo.notepad.notelist;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.AbstractList;

/**
 * Created by alexilyin on 27.09.16.
 */

public class RecyclerViewBindingAdapter<T> extends RecyclerView.Adapter<RecyclerViewBindingAdapter.ViewBindingHolder> {

    // https://stfalcon.com/en/blog/post/faster-android-apps-with-databinding                                                                        
    // https://halfthought.wordpress.com/2016/03/23/2-way-data-binding-on-android/                                                                             

    private AbstractList<T> items;

    public interface OnItemClickListener {
        void onItemClick(long id);
    }

    @Override
    public ViewBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        new ListView(null).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        return null;
    }

    @Override
    public void onBindViewHolder(ViewBindingHolder holder, int position) {
        holder.getBinding().setVariable(position, items.get(position));
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class ViewBindingHolder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        public ViewBindingHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}