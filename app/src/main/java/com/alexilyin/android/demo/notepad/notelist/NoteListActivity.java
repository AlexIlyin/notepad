package com.alexilyin.android.demo.notepad.notelist;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.alexilyin.android.demo.notepad.common.App;
import com.alexilyin.android.demo.notepad.data.DataProvider;
import com.alexilyin.android.demo.notepad.data.NoteListDTO;
import com.alexilyin.android.demo.notepad.notedetail.NoteDetailActivity;
import com.alexilyin.android.demo.notepad.R;
import com.crashlytics.android.Crashlytics;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceActivity;

import io.fabric.sdk.android.Fabric;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class NoteListActivity
        extends MvpLceActivity<FrameLayout, List<NoteListDTO>, NoteListView, NoteListPresenter>
        implements NoteListView, SwipeRefreshLayout.OnRefreshListener, NoteListAdapter.Listener {

    List<String> noteHeaders = new ArrayList<>();
    FloatingActionButton fbtnNewNote;
    SwipeRefreshLayout swipeRefreshLayout;
    private NoteListAdapter notesAdapter;

    @Inject
    DataProvider notesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) this.getApplicationContext()).component().inject(this);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_notelist);


        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout)).setTitle("MyTitle");
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(this);
//        notesData = FileDataProvider.getInstance(this);

        RecyclerView noteslist = (RecyclerView) findViewById(R.id.noteslist);
//        notesAdapter = new ArrayAdapter<String>(this, R.layout.notelist_item, R.id.list_item_text, noteHeaders);
        notesAdapter = new NoteListAdapter(this);
        notesAdapter.setListener(this);
        noteslist.setLayoutManager(new LinearLayoutManager(this));
        noteslist.setAdapter(notesAdapter);
        noteslist.addItemDecoration(
                new NoteListItemDivider(
                        AppCompatResources.getDrawable(this, R.drawable.note_list_divider)));

        fbtnNewNote = (FloatingActionButton) findViewById(R.id.new_note_btn);
        fbtnNewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewNote();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onRestart();
        loadData(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        notesData.close();
    }

    private void addNewNote() {
        NoteDetailActivity.start(this);
    }


    // MVP basic

    @NonNull
    @Override
    public NoteListPresenter createPresenter() {
        return new NoteListPresenter(this);
    }

    // Lce


    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(List<NoteListDTO> data) {
        notesAdapter.setNotes(data);
        notesAdapter.notifyDataSetChanged();
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().updateNoteList(pullToRefresh);
    }


    // Swipe

    @Override
    public void onRefresh() {
        loadData(true);
    }

    // Lce

    @Override
    public void showContent() {
        super.showContent();
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        swipeRefreshLayout.setRefreshing(false);
    }

    // RecyclerView callbacks

    @Override
    public void OnClick(NoteListDTO noteListItem) {
        NoteDetailActivity.start(this, noteListItem.getId());
    }

    @Override
    public boolean OnLongClick(NoteListDTO noteListItem) {
        return presenter.deleteNote(noteListItem.getId());
    }

    public class NoteListItemDivider extends RecyclerView.ItemDecoration {

        private Drawable divider;

        public NoteListItemDivider(Drawable divider) {
            this.divider = divider;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            if (parent.getChildAdapterPosition(view) == 0)
                return;
            outRect.top = divider.getIntrinsicHeight();
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int dividerLeft = parent.getPaddingLeft();
            int dividerRight = parent.getPaddingRight();
            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int dividerTop = child.getBottom() + params.bottomMargin;
                int dividerBottom = dividerTop + divider.getIntrinsicHeight();

                divider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
                divider.draw(c);
            }

        }
    }


}
