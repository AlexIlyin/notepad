package com.alexilyin.android.demo.notepad.common;

import android.app.Application;

import com.alexilyin.android.demo.notepad.data.DataModule;
import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.fabric.sdk.android.Fabric;

/**
 * Created by alexilyin on 13.10.16.
 */

public class App extends Application {

    private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        refWatcher = LeakCanary.install(this);
        Fabric.with(this, new Crashlytics());
    }

    protected AppComponent component;

    private AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this, refWatcher))
                .dataModule(new DataModule())
                .build();
    }


    public AppComponent component() {
        if (component == null) {
            synchronized (this) {
                if (component == null) {
                    component = createComponent();
                }
            }
        }
        return component;
    }

}
