package com.alexilyin.android.demo.notepad.data.file;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Environment;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.inject.Inject;

/**
 * Created by alexilyin on 14.10.16.
 */

public class StorageHelper {

    public static boolean isPathsEqual(File path1, File path2) {
        try {
            return path1.getCanonicalPath().equals(path2.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    // =========================================================================================
    // Public methods
    // =========================================================================================

    public static void writeObjectToFilesDir(Context context, String filename, Object obj) {
        File rootFolder = context.getApplicationContext().getFilesDir();
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromFilesDir(Context context, String filename) {
        File rootFolder = context.getApplicationContext().getFilesDir();
        return readObject(rootFolder, filename);
    }

    public static void writeObjectToCacheDir(Context context, String filename, Object obj) {
        File rootFolder = context.getApplicationContext().getCacheDir();
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromCacheDir(Context context, String filename) {
        File rootFolder = context.getApplicationContext().getCacheDir();
        return readObject(rootFolder, filename);
    }

    @TargetApi(21)
    public static void writeObjectToCodeCacheDir(Context context, String filename, Object obj) {
        if (Build.VERSION.SDK_INT >= 21) {
            File rootFolder = context.getApplicationContext().getCodeCacheDir();
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(21)
    public static Object readObjectFromCodeCacheDir(Context context, String filename) {
        if (Build.VERSION.SDK_INT >= 21) {
            File rootFolder = context.getApplicationContext().getCodeCacheDir();
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

//    String databaseName = null;
//    result[0] = context.getDatabasePath(databaseName);

    public static void writeObjectToDir(Context context, String filename, Object obj, String dirName) {
        File rootFolder = context.getApplicationContext().getDir(dirName, Context.MODE_PRIVATE);
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromDir(Context context, String filename, String dirName) {
        File rootFolder = context.getApplicationContext().getDir(dirName, Context.MODE_PRIVATE);
        return readObject(rootFolder, filename);
    }

    @TargetApi(8)
    public static void writeObjectToExternalCacheDir(Context context, String filename, Object obj) {
        if (Build.VERSION.SDK_INT >= 8) {
            File rootFolder = context.getApplicationContext().getExternalCacheDir();
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(19)
    public static void writeObjectToExternalCacheDirs(Context context, String filename, Object obj, int dirNo) {
        if (Build.VERSION.SDK_INT >= 19) {
            File rootFolder = context.getApplicationContext().getExternalCacheDirs()[dirNo];
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(8)
    public static Object readObjectFromExternalCacheDir(Context context, String filename) {
        if (Build.VERSION.SDK_INT >= 8) {
            File rootFolder = context.getApplicationContext().getExternalCacheDir();
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(19)
    public static Object readObjectFromExternalCacheDirs(Context context, String filename, int dirNo) {
        if (Build.VERSION.SDK_INT >= 19) {
            File rootFolder = context.getApplicationContext().getExternalCacheDirs()[dirNo];
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(8)
    public static void writeObjectToExternalFilesDir(Context context, String filename, Object obj, String dirType) {
        if (Build.VERSION.SDK_INT >= 8 && checkExternalDirType(dirType)) {
            File rootFolder = context.getApplicationContext().getExternalFilesDir(dirType);
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(19)
    public static void writeObjectToExternalFilesDirs(Context context, String filename, Object obj, String dirType, int dirNo) {
        if (Build.VERSION.SDK_INT >= 19 && checkExternalDirType(dirType)) {
            File rootFolder = context.getApplicationContext().getExternalFilesDirs(dirType)[dirNo];
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(8)
    public static Object readObjectFromExternalFilesDir(Context context, String filename, String dirType) {
        if (Build.VERSION.SDK_INT >= 8 && checkExternalDirType(dirType)) {
            File rootFolder = context.getApplicationContext().getExternalFilesDir(dirType);
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(19)
    public static Object readObjectFromExternalFilesDirs(Context context, String filename, String dirType, int dirNo) {
        if (Build.VERSION.SDK_INT >= 19 && checkExternalDirType(dirType)) {
            File rootFolder = context.getApplicationContext().getExternalFilesDirs(dirType)[dirNo];
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    private static boolean checkExternalDirType(String type) {
        if (Build.VERSION.SDK_INT >= 8) {
            return (type.equals(Environment.DIRECTORY_ALARMS) ||
                    type.equals(Environment.DIRECTORY_MOVIES) ||
                    type.equals(Environment.DIRECTORY_MUSIC) ||
                    type.equals(Environment.DIRECTORY_NOTIFICATIONS) ||
                    type.equals(Environment.DIRECTORY_PICTURES) ||
                    type.equals(Environment.DIRECTORY_PODCASTS) ||
                    type.equals(Environment.DIRECTORY_RINGTONES));
        } else return false;
    }

    @TargetApi(21)
    public static void writeObjectToExternalMediaDirs(Context context, String filename, Object obj, int dirNo) {
        if (Build.VERSION.SDK_INT >= 21) {
            File rootFolder = context.getApplicationContext().getExternalMediaDirs()[dirNo];
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(21)
    public static Object readObjectFromExternalMediaDirs(Context context, String filename, int dirNo) {
        if (Build.VERSION.SDK_INT >= 21) {
            File rootFolder = context.getApplicationContext().getExternalMediaDirs()[dirNo];
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    public static void writeObjectToFileStreamPath(Context context, String filename, Object obj) {
        File rootFolder = context.getApplicationContext().getFileStreamPath(filename);
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromFileStreamPath(Context context, String filename) {
        File rootFolder = context.getApplicationContext().getFileStreamPath(filename);
        return readObject(rootFolder, filename);
    }

    @TargetApi(21)
    public static void writeObjectToNoBackupFilesDir(Context context, String filename, Object obj) {
        if (Build.VERSION.SDK_INT >= 21) {
            File rootFolder = context.getApplicationContext().getNoBackupFilesDir();
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(21)
    public static Object readObjectFromNoBackupFilesDir(Context context, String filename) {
        if (Build.VERSION.SDK_INT >= 21) {
            File rootFolder = context.getApplicationContext().getNoBackupFilesDir();
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(11)
    public static void writeObjectToObbDir(Context context, String filename, Object obj) {
        if (Build.VERSION.SDK_INT >= 11) {
            File rootFolder = context.getApplicationContext().getObbDir();
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(11)
    public static Object readObjectFromObbDir(Context context, String filename) {
        if (Build.VERSION.SDK_INT >= 11) {
            File rootFolder = context.getApplicationContext().getObbDir();
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(19)
    public static void writeObjectToObbDirs(Context context, String filename, Object obj, int dirNo) {
        if (Build.VERSION.SDK_INT >= 19) {
            File rootFolder = context.getApplicationContext().getObbDirs()[dirNo];
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(19)
    public static Object readObjectFromObbDirs(Context context, String filename, int dirNo) {
        if (Build.VERSION.SDK_INT >= 19) {
            File rootFolder = context.getApplicationContext().getObbDirs()[dirNo];
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    public static void writeObjectToEnvDataDirectory(String filename, Object obj) {
        File rootFolder = Environment.getDataDirectory();
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromEnvDataDirectory(String filename) {
        File rootFolder = Environment.getDataDirectory();
        return readObject(rootFolder, filename);
    }

    public static void writeObjectToEnvDownloadCacheDirectory(String filename, Object obj) {
        File rootFolder = Environment.getDownloadCacheDirectory();
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromEnvDownloadCacheDirectory(String filename) {
        File rootFolder = Environment.getDownloadCacheDirectory();
        return readObject(rootFolder, filename);
    }

    public static void writeObjectToEnvExternalStorageDirectory(String filename, Object obj) {
        File rootFolder = Environment.getExternalStorageDirectory();
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromEnvExternalStorageDirectory(String filename) {
        File rootFolder = Environment.getExternalStorageDirectory();
        return readObject(rootFolder, filename);
    }

    @TargetApi(8)
    public static void writeObjectToExternalStoragePublicDirectory(String filename, @NotNull String dirType, Object obj) {
        if (Build.VERSION.SDK_INT >= 8 && checkPublicDirType(dirType)) {
            File rootFolder = Environment.getExternalStoragePublicDirectory(dirType);
            writeObject(rootFolder, filename, obj);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    @TargetApi(8)
    public static Object readObjectFromExternalStoragePublicDirectory(String filename, @NotNull String dirType) {
        if (Build.VERSION.SDK_INT >= 8 && checkPublicDirType(dirType)) {
            File rootFolder = Environment.getExternalStoragePublicDirectory(dirType);
            return readObject(rootFolder, filename);
        } else {
            throw new UnsupportedOperationException(); // throw exception or return null
        }
    }

    private static boolean checkPublicDirType(String type) {
        if (Build.VERSION.SDK_INT >= 8) {
            return (type.equals(Environment.DIRECTORY_ALARMS) ||
                    type.equals(Environment.DIRECTORY_DCIM) ||
                    type.equals(Environment.DIRECTORY_DOWNLOADS) ||
                    type.equals(Environment.DIRECTORY_MOVIES) ||
                    type.equals(Environment.DIRECTORY_MUSIC) ||
                    type.equals(Environment.DIRECTORY_NOTIFICATIONS) ||
                    type.equals(Environment.DIRECTORY_PICTURES) ||
                    type.equals(Environment.DIRECTORY_PODCASTS) ||
                    type.equals(Environment.DIRECTORY_RINGTONES));
        } else if (Build.VERSION.SDK_INT >= 19) {
            return (type.equals(Environment.DIRECTORY_DOCUMENTS));
        } else return false;
    }

    public static void writeObjectToEnvRootDirectory(String filename, Object obj) {
        File rootFolder = Environment.getRootDirectory();
        writeObject(rootFolder, filename, obj);
    }

    public static Object readObjectFromEnvRootDirectory(String filename) {
        File rootFolder = Environment.getRootDirectory();
        return readObject(rootFolder, filename);
    }


    // =========================================================================================
    // Public Common
    // =========================================================================================


    public static void writeObject(File rootFolder, String filename, Object obj) {
        File file = new File(rootFolder.getAbsolutePath() + "/" + filename);
        writeObject(file, obj);
    }

    public static Object readObject(File rootFolder, String filename) {
        File file = new File(rootFolder.getAbsolutePath() + "/" + filename);
        return readObject(file);
    }

    // filename: "myfile.txt" "myfolder1/folder2/myfile.txt"
    public static void writeObject(File file, Object obj) {
        File folder = file.getParentFile();
        if (folder != null) folder.mkdirs();
        writeObjectJava(file, obj);
    }

    public static Object readObject(File file) {
        return readObjectJava(file);
    }

    // =========================================================================================
    // Android low level
    // =========================================================================================


    private static void writeObjectAndroid(Context context, String filename, int mode, Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            writeObjectAndroid19(context, filename, mode, obj);
        } else {
            writeObjectAndroid1(context, filename, mode, obj);
        }
    }

    private static void writeObjectAndroid1(Context context, String filename, int mode, Object obj) {

        if (mode != Context.MODE_PRIVATE && mode != Context.MODE_APPEND) {
            throw new IllegalArgumentException();
        }

        ObjectOutputStream output = null;

        try {
            output = new ObjectOutputStream(
                    new BufferedOutputStream(
                            context.openFileOutput(filename, Context.MODE_PRIVATE)
                    )
            );

            output.writeObject(obj);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @TargetApi(19)
    private static void writeObjectAndroid19(Context context, String filename, int mode, Object obj) {

        if (mode != Context.MODE_PRIVATE && mode != Context.MODE_APPEND) {
            throw new IllegalArgumentException();
        }

        try (
                ObjectOutputStream output = new ObjectOutputStream(
                        new BufferedOutputStream(
                                context.openFileOutput(filename, Context.MODE_PRIVATE)
                        )
                )
        ) {
            output.writeObject(obj);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Object readObjectAndroid(Context context, String filename) {
        if (Build.VERSION.SDK_INT >= 19) {
            return readObjectAndroid19(context, filename);
        } else {
            return readObjectAndroid1(context, filename);
        }
    }

    private static Object readObjectAndroid1(Context context, String filename) {

        Object result = null;
        ObjectInputStream input = null;
        try {
            input = new ObjectInputStream(
                    new BufferedInputStream(
                            context.openFileInput(filename)
                    )
            );

            result = input.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @TargetApi(19)
    private static Object readObjectAndroid19(Context context, String filename) {

        Object result = null;
        try (
                ObjectInputStream input = new ObjectInputStream(
                        new BufferedInputStream(
                                context.openFileInput(filename)
                        )
                )
        ) {
            result = input.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }


    // =========================================================================================
    // Java low level
    // =========================================================================================


    private static void writeObjectJava(File file, Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            writeObjectJava19(file, obj);
        } else {
            writeObjectJava1(file, obj);
        }
    }

    private static void writeObjectJava1(File file, Object obj) {

        // http://www.javapractices.com/topic/TopicAction.do?Id=57
        // http://stackoverflow.com/questions/4092914/java-try-catch-finally-best-practices-while-acquiring-closing-resources

        ObjectOutputStream output = null;

        try {
            output = new ObjectOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream(file)
                    )
            );

            output.writeObject(obj);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @TargetApi(19)
    private static void writeObjectJava19(File file, Object obj) {

        try (
                ObjectOutputStream output = new ObjectOutputStream(
                        new BufferedOutputStream(
                                new FileOutputStream(file)
                        )
                )
        ) {
            output.writeObject(obj);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Object readObjectJava(File file) {
        if (Build.VERSION.SDK_INT >= 19) {
            return readObjectJava19(file);
        } else {
            return readObjectJava1(file);
        }
    }

    private static Object readObjectJava1(File file) {

        Object result = null;
        ObjectInputStream input = null;
        try {
            input = new ObjectInputStream(
                    new BufferedInputStream(
                            new FileInputStream(file)
                    )
            );

            result = input.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @TargetApi(19)
    private static Object readObjectJava19(File file) {

        Object result = null;
        try (
                ObjectInputStream input = new ObjectInputStream(
                        new BufferedInputStream(
                                new FileInputStream(file)
                        )
                )
        ) {
            result = input.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }

}

