package com.alexilyin.android.demo.notepad.notelist;

import android.content.Context;

import com.alexilyin.android.demo.notepad.common.App;
import com.alexilyin.android.demo.notepad.data.DataProvider;
import com.alexilyin.android.demo.notepad.data.Note;
import com.alexilyin.android.demo.notepad.data.NoteListDTO;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by alexilyin on 25.09.16.
 */

public class NoteListPresenter extends MvpBasePresenter<NoteListView> {

    @Inject
    DataProvider notesData;

    public NoteListPresenter(Context context) {
        ((App) context.getApplicationContext()).component().inject(this);
    }

    public void updateNoteList(boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        getView().setData(
                convertNoteListToNoteListDTOCollection(
                        notesData.get()));
        getView().showContent();
    }

    public boolean deleteNote(long id) {
        boolean success = notesData.remove(id);
        if (success)
            updateNoteList(false);
        return success;
    }


    // ================= Private

    private List<NoteListDTO> convertNoteListToNoteListDTOCollection(List<Note> noteList) {
        List<NoteListDTO> result = new ArrayList<>(noteList.size());
        for (Note note : noteList) {
            result.add(new NoteListDTO(note));
        }
        return result;
    }
}
