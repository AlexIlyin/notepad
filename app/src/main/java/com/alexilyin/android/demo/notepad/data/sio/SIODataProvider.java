package com.alexilyin.android.demo.notepad.data.sio;

import com.alexilyin.android.demo.notepad.data.DataProvider;
import com.alexilyin.android.demo.notepad.data.Note;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by alexilyin on 16.10.16.
 */

public class SIODataProvider implements DataProvider {

    @Inject
    public SIODataProvider() {
    }

    @Override
    public void open() {

    }

    @Override
    public List<Note> get() {
        return null;
    }

    @Override
    public Note get(long noteId) {
        return null;
    }

    @Override
    public void put(Note note) {

    }

    @Override
    public boolean remove(long noteId) {
        return false;
    }

    @Override
    public void close() {

    }
}
