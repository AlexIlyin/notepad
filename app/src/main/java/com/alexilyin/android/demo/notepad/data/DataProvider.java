package com.alexilyin.android.demo.notepad.data;

import com.alexilyin.android.demo.notepad.common.App;

import java.util.List;

/**
 * Created by alexilyin on 13.10.16.
 */

public interface DataProvider {

    void open();

    List<Note> get();

    Note get(long noteId);

    void put(Note note);

    boolean remove(long noteId);

    void close();
}
