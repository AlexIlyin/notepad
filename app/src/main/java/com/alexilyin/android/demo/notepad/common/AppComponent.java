package com.alexilyin.android.demo.notepad.common;

import android.content.Context;

import com.alexilyin.android.demo.notepad.data.DataModule;
import com.alexilyin.android.demo.notepad.data.DataProvider;
import com.alexilyin.android.demo.notepad.notedetail.NoteDetailPresenter;
import com.alexilyin.android.demo.notepad.notelist.NoteListActivity;
import com.alexilyin.android.demo.notepad.notelist.NoteListPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by alexilyin on 13.10.16.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                DataModule.class
        }
)
public interface AppComponent {

    void inject(NoteListPresenter obj);
    void inject(NoteDetailPresenter obj);
    void inject(NoteListActivity obj);

    Context context();

}
