package com.alexilyin.android.demo.notepad.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.squareup.leakcanary.RefWatcher;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by alexilyin on 13.10.16.
 */

@Module
public class AppModule {

    private final App application;
    private final RefWatcher refWatcher;

    public AppModule(App application, RefWatcher refWatcher) {
        this.application = application;
        this.refWatcher = refWatcher;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    RefWatcher provideRefWatcher() {
        return refWatcher;
    }

    @Provides
    @Singleton
    Resources provideResources() {
        return application.getResources();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("app", Context.MODE_PRIVATE);
    }

}
