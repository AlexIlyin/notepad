package com.alexilyin.android.demo.notepad.data;

/**
 * Created by alexilyin on 26.09.16.
 */

public class NoteListDTO {

    public static final long NEW_NOTE_ID = Note.NEW_NOTE_ID;

    private final long id;
    private final String title;

    public NoteListDTO(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public NoteListDTO(Note note) {
        this.id = note.getId();
        this.title = note.getTitle();
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

}
