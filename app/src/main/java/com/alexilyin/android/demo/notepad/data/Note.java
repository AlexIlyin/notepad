package com.alexilyin.android.demo.notepad.data;

import java.io.Serializable;

/**
 * Created by alexilyin on 22.09.16.
 */

public class Note implements Serializable {

    public static final long NEW_NOTE_ID = -1;

    private long id;
    private String title;
    private String text;

    public Note() {
        this(NEW_NOTE_ID, "", "");
    }

    public Note(long id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
