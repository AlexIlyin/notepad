package com.alexilyin.android.demo.notepad.notedetail;

import com.alexilyin.android.demo.notepad.data.Note;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

/**
 * Created by alexilyin on 25.09.16.
 */

public interface NoteDetailView extends MvpLceView<Note> {

}
