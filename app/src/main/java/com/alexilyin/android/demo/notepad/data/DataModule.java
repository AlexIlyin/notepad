package com.alexilyin.android.demo.notepad.data;

import android.content.Context;

import com.alexilyin.android.demo.notepad.data.file.FileDataProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by alexilyin on 13.10.16.
 */

@Module
public class DataModule {

    @Singleton
    @Provides
    DataProvider provideDataProvider(Context context) {
        return new FileDataProvider(context);
    }
}
