package com.alexilyin.android.demo.notepad.notelist;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;

import com.alexilyin.android.demo.notepad.R;
import com.alexilyin.android.demo.notepad.data.NoteListDTO;
import com.alexilyin.android.demo.notepad.databinding.NotelistItemBinding;

import java.util.List;

/**
 * Created by alexilyin on 25.09.16.
 */

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteListHolder> {

    public interface Listener {
        void OnClick(NoteListDTO noteListItem);

        boolean OnLongClick(NoteListDTO noteListItem);
    }

    private List<NoteListDTO> notes;
    private NoteListActivity context;
    private Listener listener;

    public NoteListAdapter(NoteListActivity context) {
        this.context = context;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public NoteListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.notelist_item,
                parent,
                false);

        return new NoteListHolder(viewDataBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(NoteListHolder holder, int position) {
        holder.binding.setNoteListDTO(notes.get(position));
    }

    void setNotes(List<NoteListDTO> notes) {
        this.notes = notes;
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    class NoteListHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private NotelistItemBinding binding;

        public NoteListHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            v.setBackgroundColor(Color.GREEN);
//            ViewAnimationUtils.createCircularReveal(
//                    v,
//                    v.getWidth() / 2,
//                    v.getHeight() / 2,
//                    0,
//                    (int) Math.hypot(v.getWidth() / 2, v.getHeight() / 2)).start();

            listener.OnClick(binding.getNoteListDTO());
        }

        @Override
        public boolean onLongClick(View v) {
            return listener.OnLongClick(binding.getNoteListDTO());
        }
    }

}
