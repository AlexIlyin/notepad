package com.alexilyin.android.demo.notepad.notelist;

import com.alexilyin.android.demo.notepad.data.NoteListDTO;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

/**
 * Created by alexilyin on 25.09.16.
 */

public interface NoteListView extends MvpLceView<List<NoteListDTO>> {

}
