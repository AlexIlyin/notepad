package com.alexilyin.android.demo.notepad.notedetail;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import com.alexilyin.android.demo.notepad.R;
import com.alexilyin.android.demo.notepad.data.Note;
import com.alexilyin.android.demo.notepad.databinding.ActivityNoteDetailBinding;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceActivity;

/**
 * Created by alexilyin on 22.09.16.
 */

public class NoteDetailActivity extends MvpLceActivity<LinearLayout, Note, NoteDetailView, NoteDetailPresenter> implements NoteDetailView {


    private static final String INTENT_EXTRA_NOTE_ID = "note_no";
    private static final long INTENT_EXTRA_NOTE_NO_DEFAULT = Note.NEW_NOTE_ID;

    private ViewModel viewModel;
    private ActivityNoteDetailBinding binding;

    long noteId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_note_detail);

        viewModel = new ViewModel();
        binding.setViewModel(viewModel);

        Intent intent = getIntent();
        noteId = intent.getLongExtra(
                INTENT_EXTRA_NOTE_ID,
                INTENT_EXTRA_NOTE_NO_DEFAULT);

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData(false);
    }

    // Start activity to create new Note
    public static void start(Activity context) {
        start(context, Note.NEW_NOTE_ID);
    }

    // Start activity to view/edit Note
    public static void start(Activity context, long noteId) {
        Intent intent = new Intent(context, NoteDetailActivity.class);
        intent.putExtra(INTENT_EXTRA_NOTE_ID, noteId);
//        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(context, (Pair<View,String>)null).toBundle();
//        context.startActivity(intent, bundle);
        context.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (viewModel.isEditMode.get()) {
            saveNote();
            viewModel.isEditMode.set(false);
        } else {
            super.onBackPressed();
        }
    }


    @NonNull
    @Override
    public NoteDetailPresenter createPresenter() {
        return new NoteDetailPresenter(this);
    }

    private void saveNote() {
        presenter.addNote(viewModel.getNote());
    }

    // Lce

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(Note data) {
        viewModel.setNote(data);
        if (data.getId() == Note.NEW_NOTE_ID) {
            viewModel.isEditMode.set(true);
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getNote(noteId);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override
    public void showContent() {
        super.showContent();
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
    }

    // ----

    public static class ViewModel extends BaseObservable {

        public long id;

        @Bindable
        public final ObservableField<String> title = new ObservableField<>();

        @Bindable
        public final ObservableField<String> text = new ObservableField<>();

        @Bindable
        public final ObservableBoolean isEditMode = new ObservableBoolean();


        public ViewModel() {
        }

        public void setNote(Note note) {
            this.id = note.getId();
            title.set(note.getTitle());
            text.set(note.getText());
        }

        public Note getNote() {
            return new Note(
                    id,
                    title.get(),
                    text.get()
            );
        }

        public void onEditPressed(View v) {
            isEditMode.set(true);
        }
    }
}
