package com.alexilyin.android.demo.notepad.data.file;

import android.content.Context;

import com.alexilyin.android.demo.notepad.data.DataProvider;
import com.alexilyin.android.demo.notepad.data.Note;
import com.alexilyin.android.demo.notepad.data.file.StorageHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static java.lang.Thread.sleep;

/**
 * Created by alexilyin on 22.09.16.
 */

public class FileDataProvider implements DataProvider {

    private static final String DEFAULT_FOLDER = "notes";
    private static final String DEFAULT_FILE_PREFIX = "note";
    private static final int DEFAULT_FILE_SALT = 0;

    private List<Note> notes = new ArrayList<>();
    private String filePrefix;

    private final Context appContext;
    private final File dataFolder;
    private final FilenameFilter dataFilter;

    @Inject
    public FileDataProvider(Context context) {
        appContext = context.getApplicationContext();
        filePrefix = DEFAULT_FILE_PREFIX + "_" + DEFAULT_FILE_SALT + "_";
        dataFolder = new File(appContext.getFilesDir().getAbsolutePath() + "/" + DEFAULT_FOLDER);
        dataFilter = (dir, name) -> StorageHelper.isPathsEqual(dir, dataFolder) && name.startsWith(filePrefix);
        open();
    }

    @Override
    public void open() {
        storageInit();
        delay(1);
    }

    @Override
    public List<Note> get() {
        delay(1);
        return notes;
    }

    @Override
    public Note get(long noteId) {
        delay(1);
        if (noteId < Integer.MAX_VALUE)
            return notes.get((int) noteId);
        else
            return null;
    }

    @Override
    public void put(Note note) {
        long noteId = note.getId();

        if (noteId == Note.NEW_NOTE_ID || noteId < 0 || noteId >= notes.size()) { // new note
            note.setId(notes.size());
            notes.add(note);
        } else {
            if (noteId < Integer.MAX_VALUE) {
                notes.get((int) noteId).setTitle(note.getTitle());
                notes.get((int) noteId).setText(note.getText());
            }
        }
        delay(1);
    }

    @Override
    public boolean remove(long noteId) {
        notes.remove(noteId);
        delay(1);
        return true;
    }

    @Override
    public void close() {
        storageWrite();
        delay(1);
    }

    // ========================================================================================
    // Storage-related methods
    // ========================================================================================

    // CRUD Storage

    // init storage, load data
    boolean storageInit() {
        if (dataFolder.exists()) return storageRead();
        else return dataFolder.mkdirs();
    }

    // load data from files
    boolean storageRead() {
        for (File f : dataFolder.listFiles(dataFilter)) {
            Object o = StorageHelper.readObject(f);
            if (o == null || !(o instanceof Note) || !notes.add((Note) o)) return false;
        }
        return true;
    }

    // write data to files
    boolean storageWrite() {
        storageClean();
        for (int i = 0; i < notes.size(); i++) {
            Note n = notes.get(i);
            StorageHelper.writeObject(dataFolder, filePrefix + i, n);
        }
        return true;
    }

    // delete files
    boolean storageClean() {
        for (File f : dataFolder.listFiles(dataFilter)) {
            if (!f.delete()) return false;
        }
        return true;
    }


    // =========================================================================================

    private void delay(int sec) {
//        try {
//            sleep(sec * 1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    private List<Note> generateDummyData() {
        List<Note> data = new ArrayList<>();
        for (int i = 0; i < 16; i++) {
            int randomNumber = (int) (Math.random() * 1000);
            String title = "Note title " + String.valueOf(randomNumber);
            String text = "Note text " + String.valueOf(randomNumber);
            data.add(new Note(notes.size(), title, text));
        }
        return data;
    }
}