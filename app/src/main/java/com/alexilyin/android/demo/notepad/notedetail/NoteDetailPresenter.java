package com.alexilyin.android.demo.notepad.notedetail;

import android.content.Context;

import com.alexilyin.android.demo.notepad.common.App;
import com.alexilyin.android.demo.notepad.data.DataProvider;
import com.alexilyin.android.demo.notepad.data.Note;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import javax.inject.Inject;

import static com.alexilyin.android.demo.notepad.data.Note.NEW_NOTE_ID;

/**
 * Created by alexilyin on 25.09.16.
 */

public class NoteDetailPresenter extends MvpBasePresenter<NoteDetailView> {

    @Inject
    DataProvider notesData;

    public NoteDetailPresenter(Context context) {
        ((App) context.getApplicationContext()).component().inject(this);
    }

//    public NoteDetailPresenter(Context context) {
//        notesData = FileDataProvider.getInstance(context);
//    }

    public void getNote(long noteId) {
        getView().showLoading(false);
        if (noteId == NEW_NOTE_ID) {
            getView().setData(new Note());
        } else {
            getView().setData(notesData.get(noteId));
        }
        getView().showContent();
    }

    public void addNote(Note note) {
        notesData.put(note);
    }
}
